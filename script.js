class Dog {
    constructor (name, breed, isGoodBoy){
        this.name = name;
        this.breed = breed;
        this.isGoodBoy = isGoodBoy;
    }
    get name(){
        return this.name
    }
    set name(name){
        this.name = name
    }
    get breed(){
        return this.breed
    }
    set breed(breed){
        this.breed = breed
    }
    get isGoodBoy(){
        return this.isGoodBoy
    }
    set isGoodBoy(isGoodBoy){
        this.isGoodBoy = isGoodBoy
    }
    
 }
 
 class NewDog extends Dog {
    constructor (sit, fetch){
        super(name, breed, isGoodBoy)
    }    
    sit() {
        // sitting code here
    }
    fetch() {
        // fetching code here
    }


 }
 class Traveler{
    constructor (name){
        this.name = name   
        this.food = 1
        this.isHealthy = true
        // this.hunt()
        // this.eat()
    }
    get name(){
        return this.name
    }
    set name(name){
        this.name = name
    }
    get food(){
        return this.food
    }
    set food(food){
        this.food = food
    }
    get isHealthy(){
        return this.isHealthy
    }
    set isHealthy(isHealthy){
        this.isHealthy = isHealthy
    }
    hunt() {
        this.food += 2 
    }
    eat(){
        if(this.food <= 0){
            this.isHealthy = false 
        }else{
            this.food -= 1
        }    
    }    
 }
 class Wagon {
    constructor (capacity) {
        this.capacity = Number(capacity)
        this.passagers = []
        // this.getAvailableSeatCount()
        // this.join()
        // this.shouldQuarantine()
        // this.totalFood()
    }
    get passagers(){
        return this.passagers
    }
    set passagers(passagers){
        this.passagers = passagers
    }
    get capacity(){
        return this.capacity
    }
    set capacity(capacity){
        this.capacity = capacity
    }
    getAvailableSeatCount() {
        return  (this.capacity - this.passagers.length) 
    }
    join() {
        if (this.capacity <= this.passagers.length){
            return "no space, sorry"
        }else{
            this.passagers.push(arguments)
        }
    }
    shouldQuarantine() {
        this.passagers.forEach(function checkHealthy(element, index, array){
          
            if(element.isHealthy === false){
                return false
            }
        });
        return true
    }
    totalFood() {
        let travalersFood = 0
        this.passagers.forEach(function checkHealthy(element, index, array){
           travalersFood += element[0].food
        });
        return travalersFood
    } 
        
 }
//  Traveler.prototype = {
//     constructor: Traveler,
//     hunt: function () {
//         this.food += 2 
//     },
//     eat: function () {
//         if(this.food <= 0){
//             this.isHealthy = false 
//         }else{
//             this.food -= 1
//         }    
//     }
//  }
 
//  Wagon.prototype = {
//     constructor: Wagon,
//     getAvailableSeatCount: function () {
//         return (this.capacity - this.passagers.length) 
//     },
//     join: function () {
//         if (this.capacity <= this.passagers.length){
//             return "no space, sorry"
//         }else{
//             this.passagers.push(arguments)
//         }
//     },
//     shouldQuarantine: function () {
//         this.passagers.forEach(function checkHealthy(element, index, array){
          
//             if(element.isHealthy === false){
//                 return false
//             }
//         });
//         return true
//     },
//     totalFood: function () {
//         let travalersFood = 0
//         this.passagers.forEach(function checkHealthy(element, index, array){
//            travalersFood += element[0].food
//         });
//         return travalersFood
//     }
//  }
 
// Criar uma carroça que comporta 2 pessoas
let wagon = new Wagon(2);
// Criar três viajantes
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let maude = new Traveler('Maude');

console.log(`${wagon.getAvailableSeatCount()} should be 2`);

wagon.join(henrietta);
console.log(`${wagon.getAvailableSeatCount()} should be 1`);

wagon.join(juan);
wagon.join(maude); // Não tem espaço para ela!
console.log(`${wagon.getAvailableSeatCount()} should be 0`);

henrietta.hunt(); // pega mais comida
juan.eat();
juan.eat(); // juan agora está com fome (doente)

console.log(`${wagon.shouldQuarantine()} should be true since juan is sick`);
console.log(`${wagon.totalFood()} should be 3`);